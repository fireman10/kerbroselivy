kerberos authentication for LIVY Rest API
---

This repo provides functions to invoke REST API with kerberos authentication.

If you notice some problems with this setup, please open an issue.

# A couple of Maven commands

Once you have configured your project in your IDE you can build it from there. However if you prefer you can use maven
from the command line. In that case you could be interested in this short list of commands:

* `mvn compile`: it will just compile the code of your application and tell you if there are errors
* `mvn test`: it will compile the code of your application and your tests. It will then run your tests (if you wrote
  any) and let you know if some fails
* `mvn install`: it will do everything `mvn test` does and then if everything looks file it will install the library or
  the application into your local maven repository (typically under <USER FOLDER>/.m2). In this way you could use this
  library from other projects you want to build on the same machine

If you need more information please take a look at
this [quick tutorial](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html).

# Example command to run Rest client kerberos

java -Djava.security.krb5.conf=/home/hadoop/krb5.conf -cp ./my-project-name-jar-with-dependencies.jar
class_path <path to krb5> <path to livy keytab> <livy Principal> <Rest api with kerberos authentication>

java -Djava.security.krb5.conf=/home/hadoop/krb5.conf -cp ./my-project-name-jar-with-dependencies.jar
com.dataq.hive.LivyRestAPI /home/hadoop/krb5.conf /home/hadoop/livy.keytab
livy/ip-10-158-164-218.ec2.internal@EC2.INTERNAL "http://ip-10-158-164-218.ec2.internal:8998/sessions"

# Example command to get file size for all 

java -cp ./spring-kerbrose-jar-with-dependencies.jar com.dataq.LivyRestPI.KerberoseLivyRestAPI
livy/ip-10-158-164-218.ec2.internal@EC2.INTERNAL
/home/ec2-user/livy.keytab http://ip-10-158-164-218.ec2.internal:8998/sessions /home/ec2-user/krb5.conf
\"/user/hadoop/\",\"/tmp\",\"/tmp1\"

## The sample output is

Map(/user/hadoop/ -> 1__118705444, /tmp -> EXCEPTION, /tmp1/ -> PATH_DOES_NOT_EXIST)

### if the folder is valid, will return "number of files"__"folder size"

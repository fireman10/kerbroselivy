package com.dataq.LivyRestPI;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.kerberos.client.KerberosRestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class KerberoseLivyRestAPI {

    public final String BEGIN = "###+++";
    public final String END = "+++###";

    public static void main(String[] args1) {
        try {

            APIPojo pojpo = new APIPojo(args1[0], args1[1], args1[2], args1[3]);
            final String filePaths = args1[4];

            System.out.println("###### " + filePaths);

            KerberoseLivyRestAPI a = new KerberoseLivyRestAPI();
            String output = "";
            if (args1.length == 6) {
                output = a.getFilePathsReuseSession(pojpo, filePaths, Integer.parseInt(args1[5]));
            } else {

                output = a.getFilePathsNewSession(pojpo, filePaths, true, false);

            }
            System.out.println("\n\n\noutput values are  ++++++++++++++++++++ " + output);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public String getFilePathsReuseSession(APIPojo pojpo, String filePaths, int id) throws Exception {
        try {
            KerberoseLivyRestAPI a = new KerberoseLivyRestAPI();
            String code = a.getCodeStr(filePaths);
            String res = getFilesSize(code, pojpo, id);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }


    public String getFilePathsNewSession(APIPojo pojpo, String filePaths, Boolean reuseIfSessionAvailable, Boolean deleteSession) throws Exception {
        try {

            String originalUrl = pojpo.getUrl();

            KerberoseLivyRestAPI a = new KerberoseLivyRestAPI();
            String code = a.getCodeStr(filePaths);
            String idVal = null;
            if (reuseIfSessionAvailable) {

                try {
                    idVal = a.getExistingAvailableSession(pojpo.clone());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            int id = -1;
            if (idVal == null) {
                System.out.println("Creating a new Session ....");
                id = a.getSessionId(pojpo.clone());
            } else {
                System.out.println("Found idle session ... " + idVal);
                id = (int)Double.parseDouble(idVal);
            }

            String res = getFilesSize(code, pojpo.clone(), id);
            String deleteUrl = originalUrl + "/" + id;
            pojpo.setUrl(deleteUrl);
            if (deleteSession) {
                deleteRequest(pojpo.clone());
            }

            return res;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    private String getExistingAvailableSession(APIPojo clone) {

        Map<String, Object> res = getRequest(clone);
        System.out.println(res);
        final Object sessions = res.get("sessions");
        if (sessions != null) {
            System.out.println("session are  " + sessions);
            Gson gson = new Gson();
            ArrayList<Object> objs = (ArrayList<Object>)sessions;
            for (Object obj : objs) {
                LinkedTreeMap<String,Object> livySession =  (LinkedTreeMap<String,Object>)obj; //gson.fromJson( obj.toString(), LivySession.class);
                System.out.println(livySession);
                if (livySession.get("state").equals("idle")) {
                    return livySession.get("id").toString();
                }
            }

        }
        return null;

    }


    private String getFilesSize(String code, APIPojo pojpo, int id) throws Exception {
        String url = pojpo.getUrl() + "/" + id + "/statements";
        pojpo.setUrl(url);
        pojpo.setCode(code);
        Map<String, Object> res = postRequest(pojpo);
        double statementId = Double.parseDouble(res.get("id").toString());
        final int statementId1 = (int) statementId;
        String urlSt = pojpo.getUrl() + "/" + statementId1;

        pojpo.setUrl(urlSt);

        Map<String, Object> result = checkSessionStatus(pojpo, "" + id, "waiting");
        String output = result.get("output").toString();


        String requiredString = output.substring(output.indexOf(BEGIN) + BEGIN.length(), output.indexOf(END));
        System.out.println("The final output is ########### : " + requiredString);
        return requiredString;
    }


    private String getCodeStr(String pathsStr) throws IOException, URISyntaxException {

        InputStream is = getClass().getClassLoader().getResourceAsStream("scala_code.txt");
        String lines = "";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String s = line;
                if (s.contains("LIST_OF_PATHS")) {
                    s = s.replace("LIST_OF_PATHS", pathsStr);

                }
                String ss = s.replace("\"", "\\\"");
                System.out.println("##########" + ss);
                lines += ss + "\\n";
            }
        }
        System.out.println("the code is : " + lines);
        return "\"" + lines + "\"";
    }


    private void deleteRequest(APIPojo pojo) throws MalformedURLException, URISyntaxException {
        System.setProperty("java.security.krb5.conf", pojo.getConfig());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        System.out.println("DELETE request  :::::: " + pojo.getUrl());

        KerberosRestTemplate restTemplate =
                new KerberosRestTemplate(pojo.getKeytab(), pojo.getPrincipal());
        final URI url = new URI(pojo.getUrl());
        restTemplate.delete(url); //getForObject(args[3], String.class);

    }

    private Map<String, Object> postRequest(APIPojo pojo) {
        System.setProperty("java.security.krb5.conf", pojo.getConfig());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        System.out.println("Post request  :::::: " + pojo.getUrl());

        final String json = (pojo.getCode() == null) ? "{\"kind\": \"spark\"}" : "{\"kind\": \"spark\", \"code\": " + pojo.getCode() + " }";

        System.out.println("the json is " + json);
        HttpEntity<String> request =
                new HttpEntity<String>(json, headers);

        KerberosRestTemplate restTemplate =
                new KerberosRestTemplate(pojo.getKeytab(), pojo.getPrincipal());
        String arr = restTemplate.postForObject(pojo.getUrl(), request, String.class); //getForObject(args[3], String.class);
        System.out.println(arr);
        Gson gson = new Gson();
        Map map = getMap(arr, gson);
        return map;
    }

    private Map<String, Object> getRequest(APIPojo pojo) {
        System.setProperty("java.security.krb5.conf", pojo.getConfig());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        KerberosRestTemplate restTemplate =
                new KerberosRestTemplate(pojo.getKeytab(), pojo.getPrincipal());

        System.out.println("url is : " + pojo.getUrl());

        String arr = restTemplate.getForObject(pojo.getUrl(), String.class); //getForObject(args[3], String.class);
        System.out.println(arr);
        Gson gson = new Gson();
        Map map = getMap(arr, gson);
        return map;
    }


    private Map getMap(String jsonString, Gson gson) {
        Map<String, Object> map = gson.fromJson(jsonString, Map.class);
        HashSet<String> strings1 = new HashSet<>();
        strings1.addAll(map.keySet());

        return map;
    }

    private Map<String, Object> checkSessionStatus(APIPojo pojo, String id, String starting) throws Exception {

        String url = pojo.getUrl();
        System.out.println("checkSessionStatus : and url is " + url);
        for (int i = 0; i < 20; i++) {
            try {

                Map<String, Object> m = getRequest(pojo);
                // logger.info("checking status : " + " iter :" + i + " " + m.get("state"));
                if (m.get("state").equals("idle") || m.get("state").equals("available"))
                    return m;
                else if (m.get("state").equals(starting) || m.get("state").equals("running")) {
                    Thread.sleep(5000);
                    continue;
                } else {
                    url = url.replace("status", "log");
                    pojo.setUrl(url);
                    Map<String, Object> mm = getRequest(pojo);
                    System.out.println(mm);

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private int getSessionId(APIPojo pojo) throws Exception {

        Map<String, Object> mapGet = postRequest(pojo);
        System.out.println("mapGet : " + mapGet);
        System.out.println("mapGet size : " + mapGet.size());

        mapGet.keySet().stream()
                .forEach(System.out::println);

        if (mapGet != null && mapGet.keySet().size() > 0) {
            Iterator<String> itr = mapGet.keySet().iterator();
            while (itr.hasNext()) {
                System.out.println(itr.next());
            }

            String total = mapGet.get("id").toString();
            if (total != null && ((int) Double.parseDouble(total)) > 0) {

                final int id = (int) Double.parseDouble(total);
                pojo.setUrl(pojo.getUrl() + "/" + id + "/state");
                Map<String, Object> res = checkSessionStatus(pojo, "" + id, "starting");
                return id;

            }
        }
        return -1;
    }

    private void doWithKeytabFile(String[] args) {
        System.setProperty("java.security.krb5.conf", args[0]);
        KerberosRestTemplate restTemplate =
                new KerberosRestTemplate(args[1], args[2]);
        String arr = restTemplate.getForObject(args[3], String.class);
        System.out.println(arr);
    }


}

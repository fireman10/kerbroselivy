package com.dataq.LivyRestPI;

public class APIPojo {
    String principal;
    String keytab;
    String url;
    String config;
    String code;

    public APIPojo(String principal, String keytab, String url, String config, String code) {
        this.principal = principal;
        this.keytab = keytab;
        this.url = url;
        this.config = config;
        this.code = code;
    }

    public APIPojo(String principal, String keytab, String url, String config) {
        this.principal = principal;
        this.keytab = keytab;
        this.url = url;
        this.config = config;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getKeytab() {
        return keytab;
    }

    public void setKeytab(String keytab) {
        this.keytab = keytab;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public APIPojo clone() {
        APIPojo v = new APIPojo(principal, keytab, url, config, code);
        return v;
    }
}

package com.dataq.LivyRestPI;

public class LivySession {
    private String id;
    private String state;
    private String kind;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    @Override
    public String toString() {
        return "LivySession{" +
                "id='" + id + '\'' +
                ", state='" + state + '\'' +
                ", kind='" + kind + '\'' +
                '}';
    }
}
